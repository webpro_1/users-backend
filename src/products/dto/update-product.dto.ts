import { PartialType } from '@nestjs/mapped-types';
import { CreateProductDto } from './create-product.dto';
import {
  IsNotEmpty,
  IsNumber,
  IsString,
  IsPositive,
  MinLength,
} from 'class-validator';

export class UpdateProductDto extends PartialType(CreateProductDto) {
  @MinLength(8)
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsNumber()
  @IsNotEmpty()
  price: number;
}
