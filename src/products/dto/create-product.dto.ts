import {
  IsNotEmpty,
  IsNumber,
  IsString,
  IsPositive,
  MinLength,
} from 'class-validator';

export class CreateProductDto {
  @MinLength(8)
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsNumber()
  @IsNotEmpty()
  price: number;
}
