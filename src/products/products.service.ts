import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { NOTFOUND } from 'dns';
let products: Product[] = [
  {
    id: 1,
    name: 'Americano',
    price: 55,
  },
  {
    id: 2,
    name: 'Latte coffee',

    price: 60,
  },
  {
    id: 3,
    name: 'Mocha coffee ',
    price: 65,
  },
  {
    id: 4,
    name: 'Espresso coffee ',
    price: 65,
  },
];

let lastId = 5;
@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct = {
      id: lastId++,
      ...createProductDto,
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((p) => p.id === id);
    if (index < 0) {
      throw new NotFoundException();
    }

    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((p) => p.id === id);
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct = {
      ...products[index],
      ...updateProductDto,
    };

    products[index] = updateProduct;
    return products[index];
  }

  remove(id: number) {
    const index = products.findIndex((p) => p.id === id);
    if (index < 0) {
      throw new NotFoundException();
    }
    const removeProduct = products[index];
    products.splice(index, 1);
    return removeProduct;
  }

  reset() {
    products = [
      {
        id: 1,
        name: 'Americano',
        price: 55,
      },
      {
        id: 2,
        name: 'Latte coffee',

        price: 60,
      },
      {
        id: 3,
        name: 'Mocha coffee ',
        price: 65,
      },
      {
        id: 4,
        name: 'Espresso coffee ',
        price: 65,
      },
    ];

    return 'reset product already ';
  }
}
