import { MinLength, Matches, IsNotEmpty } from 'class-validator';

export class CreateUserDto {
  @MinLength(5)
  @IsNotEmpty()
  login: string;

  @MinLength(5)
  @IsNotEmpty()
  name: string;

  @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  @MinLength(8)
  @IsNotEmpty()
  password: string;
}
