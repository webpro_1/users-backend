import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { NotFoundError } from 'rxjs';
import { NotFoundException } from '@nestjs/common/exceptions';
let users: User[] = [
  { id: 1, login: 'admin', name: 'Aministor', password: 'password' },
  { id: 2, login: 'user1', name: 'User 1', password: 'Mypassword' },
  { id: 3, login: 'user3', name: 'User 3', password: 'Mypassword3' },
];
let lastId = 4;
@Injectable()
export class UsersService {
  create(createUserDto: CreateUserDto) {
    const newUser: User = {
      id: lastId++,
      ...createUserDto,
    };

    users.push(newUser);
    return newUser;
  }

  findAll() {
    return users;
  }

  findOne(id: number) {
    const index = users.findIndex((user) => user.id == id);
    if (index < 0) {
      throw new NotFoundException();
    }
    return users[index];
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    const index = users.findIndex((user) => user.id == id);
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateUser: User = {
      ...users[index],
      ...updateUserDto,
      //ทับได้
    };
    users[index] = updateUser;
    return updateUser;
  }

  remove(id: number) {
    const index = users.findIndex((user) => user.id == id);
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteUser = users[index];
    users.splice(index, 1);
    return deleteUser;
  }

  reset() {
    users = [
      { id: 1, login: 'admin', name: 'Aministor', password: 'password' },
      { id: 2, login: 'user1', name: 'User 1', password: 'Mypassword' },
      { id: 3, login: 'user3', name: 'User 3', password: 'Mypassword3' },
    ];
    lastId = 4;
    return 'reset user already';
  }
}
